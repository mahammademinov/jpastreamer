package com.company.service;

import com.company.dto.response.TaskResponse;

import java.util.List;

public interface TaskService {

    TaskResponse findById(Long id);

    List<TaskResponse> findAllWithPagination(int offset, int limit);
}
