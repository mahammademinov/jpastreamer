package com.company.service;

import com.company.dto.response.UserResponse;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface UserService {

    List<UserResponse> findByName(String name);

    UserResponse findById(Long id);

    List<UserResponse> findAll();

    List<UserResponse> findAllWithPagination(int offset, int limit);
}
