package com.company.service;

import com.company.dto.response.UserResponse;
import com.company.entity.User;
import com.company.entity.User$;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.stream.Collectors;

import static com.speedment.jpastreamer.streamconfiguration.StreamConfiguration.of;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final JPAStreamer jpaStreamer;

    @Override
    public UserResponse findById(Long id) {
        return jpaStreamer.stream(of(User.class))
//                        .joining(User$.ta)
                .filter(User$.id.equal(id))
                .map(UserResponse::fromEntity)
                .findFirst()
                .orElseThrow();
    }

    @Override
    public List<UserResponse> findByName(String name) {
        return jpaStreamer.stream(User.class)
                .filter(user -> user.getName().equalsIgnoreCase(name))
                .sorted(User$.id.reversed())
                .map(UserResponse::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public List<UserResponse> findAllWithPagination(@PathVariable("offset") int offset,
                                                    @PathVariable("limit") int limit) {
        return jpaStreamer.stream(User.class)
                .skip(offset)
                .limit(limit)
                .map(UserResponse::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public List<UserResponse> findAll() {
        return jpaStreamer.stream(User.class)
                .map(UserResponse::fromEntity)
                .collect(Collectors.toList());
    }

}
