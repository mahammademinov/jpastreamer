package com.company.service;

import com.company.dto.response.TaskResponse;
import com.company.entity.Task;
import com.company.entity.Task$;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.stream.Collectors;

import static com.speedment.jpastreamer.streamconfiguration.StreamConfiguration.of;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final JPAStreamer jpaStreamer;

    @Override
    public TaskResponse findById(Long id) {
        return jpaStreamer.stream(of(Task.class)
                        .joining(Task$.users))
                .filter(Task$.id.equal(id))
                .map(TaskResponse::fromEntity)
                .findFirst()
                .orElseThrow();
    }

    @Override
    public List<TaskResponse> findAllWithPagination(@PathVariable("offset") int offset,
                                                    @PathVariable("limit") int limit) {
        return jpaStreamer.stream(Task.class)
                .skip(offset)
                .limit(limit)
                .map(TaskResponse::fromEntity)
                .collect(Collectors.toList());
    }
}
