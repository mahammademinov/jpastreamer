package com.company.entity;

import com.company.entity.embed.UserTaskId;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "user_task")
public class UserTask {

    @EmbeddedId
    private UserTaskId userTaskId;

    @ManyToOne
    @MapsId("userId")
    private User user;

    @ManyToOne
    @MapsId("taskId")
    private Task task;
}
