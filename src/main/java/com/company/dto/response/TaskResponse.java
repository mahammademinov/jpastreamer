package com.company.dto.response;

import com.company.entity.Task;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskResponse {

    private Long id;
    private String name;
    private List<UserTaskResponse> users;

    public static TaskResponse fromEntity(Task task) {
        return TaskResponse.builder()
                .id(task.getId())
                .name(task.getName())
                .users(task.getUsers().stream()
                        .map(UserTaskResponse::fromEntity)
                        .collect(Collectors.toList()))
                .build();
    }
}
