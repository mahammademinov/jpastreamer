package com.company.dto.response;

import com.company.entity.UserTask;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserTaskResponse {

    private Long userId;

    public static UserTaskResponse fromEntity(UserTask userTask) {
        return UserTaskResponse.builder()
                .userId(userTask.getUserTaskId().getUserId())
                .build();
    }
}
