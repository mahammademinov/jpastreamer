package com.company.dto.payload;

import com.company.entity.User;
import lombok.Data;

import java.time.LocalDate;

@Data
public class UserPayload {

    private String name;
    private String surname;
    private LocalDate birthdate;
    private String email;

    public User toEntity() {
        return User.builder()
                .name(this.name)
                .surname(this.surname)
                .birthdate(this.birthdate)
                .email(this.email)
                .build();
    }

}
