package com.company.controller;

import com.company.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/find/{name}")
    public ResponseEntity<?> findAllUsers(@PathVariable(value = "name") String name) {
        if (name != null) {
            return ResponseEntity.ok(userService.findByName(name));
        }
        return ResponseEntity.ok(userService.findAll());
    }

    @GetMapping("/offset/{offset}/limit/{limit}")
    public ResponseEntity<?> findAllWithPagination(@PathVariable("offset") int offset,
                                                   @PathVariable("limit") int limit) {
        return ResponseEntity.ok(userService.findAllWithPagination(offset, limit));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(userService.findById(id));
    }

}
