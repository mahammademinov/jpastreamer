package com.company.controller;

import com.company.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tasks")
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(taskService.findById(id));
    }

    @GetMapping("/offset/{offset}/limit/{limit}")
    public ResponseEntity<?> findAllWithPagination(@PathVariable("offset") int offset,
                                                   @PathVariable("limit") int limit) {
        return ResponseEntity.ok(taskService.findAllWithPagination(offset, limit));
    }

}
