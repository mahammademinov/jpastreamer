INSERT INTO USERS(id, name, surname, email, birthdate) VALUES(1, 'Mahammad', 'Eminov', 'mahammad.eminov@gmail.com', '2001-06-11');
INSERT INTO USERS(id, name, surname, email, birthdate) VALUES(2, 'Mahammad', 'Aliyev', 'mahammad.aliyev@gmail.com', '2001-06-11');
INSERT INTO USERS(id, name, surname, email, birthdate) VALUES(3, 'Mahammad', 'Bagirov', 'mahammad.bagirov@gmail.com', '2001-06-11');
INSERT INTO USERS(id, name, surname, email, birthdate) VALUES(4, 'Test', 'Test', 'test.test@gmail.com', '2000-02-02');
INSERT INTO USERS(id, name, surname, email, birthdate) VALUES(5, 'Ali', 'Aliyev', 'ali@gmail.com', '2000-02-02');
INSERT INTO USERS(id, name, surname, email, birthdate) VALUES(6, 'Vali', 'Valiyev', 'vali@gmail.com', '2000-02-02');


INSERT INTO TASKS(id, name) VALUES(1, 'Create task table');
INSERT INTO TASKS(id, name) VALUES(2, 'Update task table');
INSERT INTO TASKS(id, name) VALUES(3, 'Delete task table');

INSERT INTO USER_TASK(user_id, task_id) VALUES(1,1);
INSERT INTO USER_TASK(user_id, task_id) VALUES(5,3);
INSERT INTO USER_TASK(user_id, task_id) VALUES(6,2);

